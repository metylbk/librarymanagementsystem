<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Layout Language Lines
    */

    'app_name' => 'Library Management System',
    'app_name_short' => 'LMS',
];
