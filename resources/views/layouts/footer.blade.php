<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="company-intro">
                <h3>{{ trans('layout.app_name') }}</h3>
                <ul class="list-unstyled">
                    <li>E-mail: <a href="mailto:contact@lms.com">contact@lms.com</a></li>
                    <li>Copyright © 2016 lms.com</li>
                </ul>
            </div>
        </div>
    </div>
</footer>
