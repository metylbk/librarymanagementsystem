<nav class="navbar navbar-fixed-top navbar-default navbar-principal">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hiden-xs" href="/">{{ trans('layout.app_name') }}</a>
            <a class="navbar-brand visible-xs" href="/">{{ trans('layout.app_name_short') }}</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li>
                        <div class="navbar-btn">
                            <a href="/auth/register" class="btn btn-primary"><i class="fa fa-btn fa-user-plus"></i> {{ trans('auth.register') }}</a>
                        </div>
                    </li>
                    <li>
                        <div class="navbar-btn">
                            <a href="/auth/login" class="btn btn-success"><i class="fa fa-btn fa-sign-in"></i> {{ trans('auth.login') }}</a>
                        </div>
                    </li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">
                            <i class="fa fa-user"></i> {{ Auth::user()->name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/auth/password/change"><i class="fa fa-key fa-fw"></i> {{ trans('auth.change_password') }}</a></li>
                            <li class="divider"></li>
                            <li><a href="/auth/logout"><i class="fa fa-sign-out fa-fw"></i> {{ trans('auth.logout') }}</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav><!-- end top navigation -->
