@extends('layouts.master')

@section('title', trans('auth.login'))

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
            <div class="panel panel-default panel-auth">
                <div class="panel-heading">
                    <h1>{{ trans('auth.login') }}</h1>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-sm-4 control-label">{{ trans('auth.email') }}:</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ trans('auth.email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-sm-4 control-label">{{ trans('auth.password') }}:</label>

                            <div class="col-sm-6">
                                <input type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="{{ trans('auth.password') }}">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3 col-sm-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> {{ trans('auth.remember_password') }}
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-sign-in"></i> {{ trans('auth.login') }}
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-4">
                                <hr/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-4">
                                <div class="pull-right">
                                    <a href="/auth/reset/password"><i class="fa fa-btn fa-unlock"></i> {{ trans('auth.forget_password') }}?</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-4">
                                <div class="pull-right">
                                    <a href="/auth/register") }}"><i class="fa fa-btn fa-user-plus"></i> {{ trans('auth.register') }}</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
