@extends('layouts.master')

@section('title', trans('auth.reset_password'))

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
      <div class="panel panel-default panel-auth">
        <div class="panel-heading">
          <h1>{{ trans('auth.reset_password') }}</h1>
        </div>

        <div class="panel-body">
          @if (session('status'))
            <div class="alert alert-success">
              {{ session('status') }}
            </div>
          @endif
          <form class="form-horizontal" role="form" method="POST" action="{{ url('/email-dat-lai-mat-khau') }}">
            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label class="col-sm-4 control-label">{{ trans('auth.email') }}:</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ trans('auth.email') }}">
                @if ($errors->has('email'))
                  <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-6 col-sm-offset-4">
                <button type="submit" class="btn btn-primary pull-right">
                  <i class="fa fa-btn fa-envelope"></i> {{ trans('auth.send_link_reset_password') }}
                </button>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-6 col-sm-offset-4">
                <hr/>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-6 col-sm-offset-4">
                <div class="pull-right">
                  <a href="/dang-ky") }}"><i class="fa fa-btn fa-user-plus"></i> {{ trans('auth.register') }}</a>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-6 col-sm-offset-4">
                <div class="pull-right">
                  <a href="/dang-nhap"><i class="fa fa-btn fa-sign-in"></i> {{ trans('auth.login') }}</a>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
