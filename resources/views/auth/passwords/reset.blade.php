@extends('layouts.master')

@section('title', trans('auth.reset_password'))

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
      <div class="panel panel-default panel-auth">
        <div class="panel-heading">
          <h1>{{ trans('auth.reset_password') }}</h1>
        </div>

        @if (session('status'))
          <div class="alert alert-success">
            {{ session('status') }}
          </div>
        @endif

        <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST" action="{{ url('/dat-lai-mat-khau') }}">
            {!! csrf_field() !!}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label class="col-sm-4 control-label">{{ trans('auth.email') }}:</label>

              <div class="col-sm-6">
                <input type="text" class="form-control" name="email" value="{{ $email or old('email') }}" placeholder="{{ trans('auth.email') }}">

                @if ($errors->has('email'))
                  <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label class="col-sm-4 control-label">{{ trans('auth.password') }}:</label>

              <div class="col-sm-6">
                <input type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="{{ trans('auth.password') }}">

                @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
              <label class="col-sm-4 control-label">{{ trans('auth.password_confirm') }}:</label>
              <div class="col-sm-6">
                <input type="password" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="{{ trans('auth.password_confirm') }}">

                @if ($errors->has('password_confirmation'))
                  <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-6 col-sm-offset-4">
                <button type="submit" class="btn btn-primary pull-right">
                  <i class="fa fa-btn fa-refresh"></i> {{ trans('auth.reset_password') }}
                </button>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
