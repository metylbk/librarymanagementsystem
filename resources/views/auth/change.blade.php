@extends('layouts.master')

@section('title', trans('auth.change_password'))

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
      <div class="panel panel-default panel-auth">
        <div class="panel-heading">
          <h1>{{ trans('auth.change_password') }}</h1>
        </div>
        @if (session('status'))
          <div class="alert alert-success">
            {{ session('status') }}
          </div>
        @endif

        <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST" action="/doi-mat-khau">
            {!! csrf_field() !!}

            <div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">
              <label class="col-sm-4 control-label">{{ trans('auth.current_password') }}:</label>

              <div class="col-sm-6">
                <input type="password" class="form-control" name="current_password" value="{{ old('current_password') }}" placeholder="{{ trans('auth.current_password') }}">

                @if ($errors->has('current_password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('current_password') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
              <label class="col-sm-4 control-label">{{ trans('auth.new_password') }}:</label>

              <div class="col-sm-6">
                <input type="password" class="form-control" name="new_password" value="{{ old('new_password') }}" placeholder="{{ trans('auth.new_password') }}">

                @if ($errors->has('new_password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('new_password') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group{{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
              <label class="col-sm-4 control-label">{{ trans('auth.new_password_confirm') }}:</label>
              <div class="col-sm-6">
                <input type="password" class="form-control" name="new_password_confirmation" value="{{ old('new_password_confirmation') }}" placeholder="{{ trans('auth.new_password_confirm') }}">

                @if ($errors->has('new_password_confirmation'))
                  <span class="help-block">
                    <strong>{{ $errors->first('new_password_confirmation') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-6 col-sm-offset-4">
                <button type="submit" class="btn btn-primary pull-right">
                  <i class="fa fa-btn fa-refresh"></i> {{ trans('auth.change_password') }}
                </button>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
